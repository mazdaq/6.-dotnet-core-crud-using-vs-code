using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task7.Models
{
    public class ItemModel
    {
     public int id {get;set;} 
     public string name {get;set;}
     public string type {get;set;}
     public string price {get;set;}  
    }
}