﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task7.Models;
using Task7.Repository;
namespace Task7.Controllers
{
    public class HomeController : Controller
    {
        //creating Item Repository object in order to acccess CRUD operation Methods
         ItemRepo repo = new ItemRepo();
      
        public IActionResult Index()
        {
            //Get List of All Items and copy that list to a list variable
           var list = repo.GetAllItem();

           //Pass that list to Index View.
            return View(list);
        }
        
        //Update View Method
        public IActionResult Update()
        {
            //create a usermodel
            ItemModel userModel = new ItemModel();
            
            //pass model to view
            return View(userModel);
        }


        [HttpPost] //Post action for getting input from user
        public IActionResult Update(ItemModel itemModel)
        {
            //send recieved model informatin and sent to UpdateItem Method in Repository to update it
            repo.UpdateItem(itemModel);

            //Rediret to Index page after updating
            return RedirectToAction("Index");
        }

        //Delete Method
        public IActionResult delete(ItemModel model)
        {
            //calling DeleteItemMethod from Repository
            repo.deleteItem(model);
            return RedirectToAction("Index");
        }


        //Add new item - View Page
        public IActionResult New()
        {
            //creating model and passing it to view
            ItemModel userModel = new ItemModel();
            
            return View(userModel);
        }

        [HttpPost]//receied input from user and adding new item
        public IActionResult New(ItemModel itemModel)
        {
            
            //calling newitem method and passing recieve info to it. in order to save information
            repo.newItem(itemModel);
            return RedirectToAction("Index");
        }






        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
