using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task7
{
    public class DataAccessLayer
    {

        //connection string
        public string ConnectionString = "server=localhost;port=3306;database=usermgt;user=root;password=root2";
      
        //mysql connection object
        public MySqlConnection connection = new MySqlConnection();
        
        //Mysql command object
        public MySqlCommand command = new MySqlCommand();

        
        
        //Get Connection - Passing connection string
        public MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString); //passing connection string to msyqlconnection
        }

        //helper for executing Non queries.
        public void executeMyQuery(string query)
        {
            try
            {
                connection=new MySqlConnection(ConnectionString); //passing conneciotn string
                connection.Open(); //open conneciton 
                command = new MySqlCommand(query, connection); //passing queries alongside connection string

                if (command.ExecuteNonQuery() == 1)
                {
                    
                }

                else
                {
                    
                }

            }
            catch (Exception ex)
            {
             //catch exception here if any   
            }
            finally
            {
               connection.Close(); //close connection
            }
        }


    }
}