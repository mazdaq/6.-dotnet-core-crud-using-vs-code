using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using Task7.Models;
namespace Task7.Repository
{
    public class ItemRepo
    {
        //Helper - For opening and closing Database connection
        DataAccessLayer dal = new DataAccessLayer();
        
        //getting list of all itmes from database
        public List<ItemModel> GetAllItem()
        {
            List<ItemModel>itemlist = new List<ItemModel>();
            using(dal.connection = dal.GetConnection())
            {
                //open conneciton
                dal.connection.Open();

                //select query
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM item_table", dal.connection);
                  using (MySqlDataReader reader = cmd.ExecuteReader()) //ExecuteReader for reacding record
                  {
                      //if there is any record, then read and add to list
                      while (reader.Read())
                      {
                          //adding items to list if they exists
                         itemlist.Add(new ItemModel()
                         {
                            id =reader.GetInt32("id"),
                            name= reader.GetString("name"),
                            type = reader.GetString("type"),
                            price = reader.GetString("price")
                            });
                      }
                  }
            }
            //return item list if there is any
            return itemlist;
        }

        //updating item Record
        public void UpdateItem (ItemModel model)
        {
            //updating query based on information recieved and then executing it
            string query = "update item_table set name= '"+model.name+"', type='"+model.type+"', price='"+model.price+"' where id='"+model.id+"' ";
            dal.executeMyQuery(query);

        }

        //delete item record
        public void deleteItem(ItemModel model)
        {
            //delete record based on id received
            string query = "delete from item_table where id='"+model.id+"' ";
            dal.executeMyQuery(query);
        }

        //new itme record
        public void newItem(ItemModel model)
        {
            //insert new item into database record
            string query="insert into item_table(name,type,price)values('"+model.name+"','"+model.type+"','"+model.price+"')";
            dal.executeMyQuery(query);
        }


    }
}